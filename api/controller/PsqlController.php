<?php 
    include_once '../service/PsqlService.php';

    class PsqlController{
        private $code;
        private $service;

        public function __construct($code){
            $this->code = $code;
        }


        public function main(){
            $this->service = new PsqlService($this->code);
            $this->service->createEnvironmentService();
        }
        
    }
?>