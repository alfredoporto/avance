<?php 
    include_once '../dto/UserDto.php';
    include_once '../service/CreateEnvironmentService.php';

    class MainController{
        private $code;
        private $databaseType;

        public function __construct(){
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Methods: POST");
            header("Content-Type: application/json; charset=UTF-8");
            $data = json_decode(file_get_contents("php://input"));
            
            $this->code = $data->code;
            $this->databaseType = $data->databaseType;

        }
        public function main(){
            switch($this->databaseType){
                case "postgresql":
                    include_once './PsqlController.php';
                    $controller = new PsqlController($code);
                    $controller->main();
                    break;
                /*case ($databaseType == 'mysql' ):
                    $service = new MysqlService($code);
                    break;
                */
            }
        }
       
    }
?>