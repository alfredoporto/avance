<?php 
    class Dbto{
        private $host;
        private $user;
        private $pass;
        private $dbname;


        public function __construct($type){
            $this->host = $type . 'DB_HOST';
            $this->user = $type . 'DB_USER';
            $this->pass = $type . 'DB_PASS';
            $this->dbname = $type . 'DB_NAME';
        }
    }
?>