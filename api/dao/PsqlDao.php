<?php 
    include_once '../objects/User.php';
    include_once '../config/PsqlDatabase.php';
    class PsqlDao{
        private $conn;
        private $code;

        public function __construct($code){
            $this->code = $code;
            $this->conn = new PsqlDatabase();       
        }

        

        public function createEnvironmentDao(){
            $query = "SELECT public.create_environment(?)";
            $stmt = $this->conn->getConnection->prepare($query);
            $stmt->bindValue(1, $this->code, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt;
        }
    }
?>                   