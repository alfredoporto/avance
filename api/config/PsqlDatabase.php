<?php
class PsqlDatabase{
  
    // specify your own database credentials
    private $host = "3.227.138.194";
    private $db_name = "postgres";
    private $username = "postgres";
    private $password = "postgres";
    public $conn;
  
    // get the database connection
    public function getConnection(){
  
        $this->conn = null;
  
        try{
            $this->conn = new PDO("pgsql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
  
        return $this->conn;
    }
}
?>