<?php
class MysqlDatabase{
  
    // specify your own database credentials
    private $host = "localhost";
    private $db_name = "nixvoid";
    private $username = "nixvoid";
    private $password = "nixvoid";
    public $conn;
  
    // get the database connection
    public function getConnection(){
  
        $this->conn = null;
  
        try{
            $this->conn = new PDO("pgsql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
  
        return $this->conn;
    }
}
?>